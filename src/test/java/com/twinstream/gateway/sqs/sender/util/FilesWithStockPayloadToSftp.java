package com.twinstream.gateway.sqs.sender.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.UUID;

public class FilesWithStockPayloadToSftp extends BaseTestFileProducer{

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static final int MESSAGE_COUNT = 1;

    private static final StockPayloadType stockPayloadType = StockPayloadType.PAYLOAD_LARGE_SISL_01;

    public static void main(String[] args) {
        new FilesWithStockPayloadToSftp().withMessageCount(MESSAGE_COUNT)
                                         .generateAndCreate();
    }

    @Override
    protected String getMetadata(int id) {
        return new MetadataMapBuilder().withMandatoryFlowMetadata(id)
                                       .withOptionalFlowMetadata("description")
                                       .withMessageMetadataContentLength(stockPayloadType.getPayloadSize())
                                       .withMessageMetadataContentType("application/json")
                                       .withMandatoryCustomMetadata()
                                       .withOptionalCustomMetadata()
                                       .withSafeTextFlagAs(Boolean.FALSE.toString())
                                       .build();
    }

    @Override
    protected void createPayloadFile(File payloadFile, int i, UUID correlationId) throws IOException {
        new PayloadFileBuilder()
                .withStockPayload(stockPayloadType)
                .toFile(payloadFile)
                .buildStockFile();
    }
}
