package com.twinstream.gateway.sqs.sender.util;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.UUID;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;

public class PayloadFileBuilder {

    private String payloadString;

    private StockPayloadType stockPayloadType;

    private File payloadFile;

    public PayloadFileBuilder toFile(File payloadFile) {
        this.payloadFile = payloadFile;
        return this;
    }

    public PayloadFileBuilder withJsonPayload(int i, UUID correlationId, String source) {
        payloadString =  buildJsonPayload("Batch - " + LocalDateTime.now().format(ISO_LOCAL_DATE_TIME) + " - " + i,
                                          correlationId, source);
        return this;
    }

    private String buildJsonPayload(String batchName, UUID correlationId, String source) {
        return String.format("{\n"
                                     + "  \"batchName\": \"%s\",\n"
                                     + "  \"now\": \"%s\"\n"
                                     + "  \"correlationId\": \"%s\"\n"
                                     + "  \"source\": \"%s\"\n"
                                     + "}", batchName, Instant.now(), correlationId, source);
    }

    public PayloadFileBuilder withEmptyPayload(int i) {
        payloadString =  "message: " + i;
        return this;
    }

    public PayloadFileBuilder withSislPayload() {
        payloadString = buildSislPayload();
        return this;
    }

    private String buildSislPayload() {
        throw new RuntimeException("to be implemented");
    }

    public PayloadFileBuilder withPayloadFile(UUID correlationId, StockPayloadType stockPayloadType) {
        return this;
    }

    public void buildStringFile() throws IOException {
        FileUtils.writeStringToFile(payloadFile, payloadString, StandardCharsets.UTF_8.toString());
    }

    public PayloadFileBuilder withStockPayload(StockPayloadType stockPayloadType) {
        this.stockPayloadType = stockPayloadType;
        return this;
    }

    public void buildStockFile() throws IOException {
        FileUtils.copyFile(stockPayloadType.getPayloadFile(), payloadFile, false);
    }
}
