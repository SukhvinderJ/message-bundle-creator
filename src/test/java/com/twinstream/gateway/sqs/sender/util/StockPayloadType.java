package com.twinstream.gateway.sqs.sender.util;

import java.io.File;
import java.nio.file.Paths;

public enum StockPayloadType {

    PAYLOAD_ONE(Paths.get("C:\\output\\sftpServer\\one.txt").toFile(), 63L),
    PAYLOAD_LARGE_SISL_01(Paths.get("C:\\output\\sftpServer\\large-sisl-file.txt").toFile(), 4096L),
    PAYLOAD_SMALL_01(Paths.get("C:\\output\\sftpServer\\small-01.txt").toFile(), 12288L),
    PAYLOAD_MEDIUM_01(Paths.get("C:\\output\\sftpServer\\medium-01.txt").toFile(), 58593280L),
    PAYLOAD_BIG_01(Paths.get("C:\\output\\sftpServer\\big-01.txt").toFile(), 677139368L),
    PAYLOAD_VERY_BIG_FILE(Paths.get("C:\\output\\sftpServer\\very-big.iso").toFile(), 4781506560L);

    private final long payloadSize;

    private final File payloadFile;

    StockPayloadType(File payloadFile, long payloadSize) {
        this.payloadSize = payloadSize;
        this.payloadFile = payloadFile;
    }

    public long getPayloadSize() {
        return payloadSize;
    }

    public File getPayloadFile() {
        return payloadFile;
    }
}
