package com.twinstream.gateway.sqs.sender.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class FilesWithRandomStockPayloadToSftp extends BaseTestFileProducer{

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static final int MESSAGE_COUNT = 1;

    private StockPayloadType stockPayloadType;

    public static void main(String[] args) {
        new FilesWithRandomStockPayloadToSftp().withMessageCount(MESSAGE_COUNT)
                                               .generateAndCreate();
    }

    @Override
    protected String getMetadata(int id) {
        stockPayloadType = randomStockPayloadType();
        return new MetadataMapBuilder().withMandatoryFlowMetadata(id)
                                       .withOptionalFlowMetadata("description")
                                       .withMessageMetadataContentLength(stockPayloadType.getPayloadSize())
                                       .withMessageMetadataContentType("application/json")
                                       .withMandatoryCustomMetadata()
                                       .withOptionalCustomMetadata()
                                       .withSafeTextFlagAs(Boolean.FALSE.toString())
                                       .build();
    }

        @Override
    protected void createPayloadFile(File payloadFile, int i, UUID correlationId) throws IOException {
        new PayloadFileBuilder()
                .withStockPayload(stockPayloadType)
                .toFile(payloadFile)
                .buildStockFile();
    }

    protected StockPayloadType randomStockPayloadType() {
        int distribution = ThreadLocalRandom.current().nextInt(1, 100);
        if (distribution == 100) {
            return StockPayloadType.PAYLOAD_BIG_01;
        } else if (distribution >= 95) {
            return StockPayloadType.PAYLOAD_MEDIUM_01;
        } else if (distribution >= 50) {
            return StockPayloadType.PAYLOAD_SMALL_01;
        } else {
            return StockPayloadType.PAYLOAD_ONE;
        }
    }
}
