package com.twinstream.gateway.sqs.sender.util;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.UUID;

public abstract class BaseTestFileProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static final String OUTPUT_FOLDER = "C:\\output\\sqs-sender\\file-queue";

    private int messageCount = 1;

    protected BaseTestFileProducer withMessageCount(int messageCount) {
        this.messageCount = messageCount;
        return this;
    }

    protected void generateAndCreate() {

        for (int i = 0; i < getMessageCount(); i++) {
            try {
                UUID correlationId = UUID.randomUUID();
                MessageBundle messageBundle = MessageBundle.builder()
                                                           .metadataFile(Paths.get(OUTPUT_FOLDER, correlationId + ".meta").toFile())
                                                           .payloadFile(Paths.get(OUTPUT_FOLDER, correlationId + ".payload").toFile())
                                                           .build();
                createMetadataFile(i, messageBundle);
                createPayloadFile(messageBundle.getPayloadFile(), i, correlationId);
                LOGGER.info("Files created {}, correlationId: {}", i, correlationId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    protected void createMetadataFile(int i, MessageBundle messageBundle) throws IOException {
        FileUtils.writeStringToFile(messageBundle.getMetadataFile(), getMetadataString(i), StandardCharsets.UTF_8.toString());
    }

    private String getMetadataString(int i) {
        return "{ " + getMetadata(i) + " }";
    }

    protected abstract String getMetadata(int i);

    protected abstract void createPayloadFile(File payloadFile, int i, UUID correlationId) throws IOException;

    protected int getMessageCount() {
        return messageCount;
    }
}
