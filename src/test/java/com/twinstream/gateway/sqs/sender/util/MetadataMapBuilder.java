package com.twinstream.gateway.sqs.sender.util;

import org.apache.commons.lang3.tuple.ImmutablePair;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class MetadataMapBuilder {

    Map<String, String> metadata;

    public MetadataMapBuilder() {
        this.metadata = new HashMap<>();
    }

    public MetadataMapBuilder withMandatoryFlowMetadata(int id) {
//        metadata.put("x-xd-flow-id", UUID.randomUUID().toString());
        metadata.put("x-xd-flow-id", "f8edd34e-865e-4bb5-8e4c-43e2ebb42520");
        metadata.put("x-xd-job-id", UUID.randomUUID().toString());
        metadata.put("x-xd-classification", "OFFICIAL");
        metadata.put("x-xd-job-timestamp", Instant.now().toString());
        return this;
    }

    public MetadataMapBuilder withOptionalFlowMetadata(String description) {
        metadata.put("x-filename", "testFilename");
        metadata.put("x-xd-job-reference", UUID.randomUUID().toString());
        metadata.put("x-xd-job-description", description);
        return this;
    }

    public MetadataMapBuilder withMessageMetadataContentType(String contentType) {
        metadata.put("content-type", contentType);
        return this;
    }

    public MetadataMapBuilder withMessageMetadataContentLength(long contentLength) {
        metadata.put("content-length", String.valueOf(contentLength));
        return this;
    }

    public MetadataMapBuilder withMandatoryMessageMetadata() {
        metadata.put("content-type", "application/json");
        metadata.put("content-length", "99");
        return this;
    }

    public MetadataMapBuilder withMandatoryCustomMetadata() {
        metadata.put("custom-key1", "custom-value1");
        metadata.put("custom-key2", "custom-value2");
        return this;
    }

    public MetadataMapBuilder withOptionalCustomMetadata() {
        metadata.put("custom-key3", "custom-value3");
        return this;
    }

    public String build() {
        return  metadata.entrySet()
                        .stream()
                        .map(entry -> format(entry.getKey(), entry.getValue()))
                        .collect(Collectors.joining(", "));
    }

    public MetadataMapBuilder withSafeTextFlagAs(String value) {
        metadata.put("x-xd-sqs-safe-text", value);
        return this;
    }

    public static String format(String key, String value) {
        return ImmutablePair.of(key, value).toString("%s: !s \"%s\"");
    }
}
