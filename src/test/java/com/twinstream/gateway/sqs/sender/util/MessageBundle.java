package com.twinstream.gateway.sqs.sender.util;

import org.immutables.value.Value;

import java.io.File;

@Value.Immutable
public interface MessageBundle {

    static ImmutableMessageBundle.Builder builder() {
        return ImmutableMessageBundle.builder();
    }

    File getMetadataFile();

    File getPayloadFile();

    default boolean filesExist() {
        return getMetadataFile().exists() && getPayloadFile().exists();
    }
}
